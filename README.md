Role Name
=========

Custom OS Changes

Requirements
------------

N/A

Role Variables
--------------

Optional:
extra_packages - comma separates list of packages
docker - docker-ce installation under Debian 9 (only!)

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
- name: OS cumstomization
  hosts: all
  #strategy: debug
  gather_facts: yes
  become: yes
  roles:
    - { role: ansible-os-customization extra_packages: docker,git , tags: [ 'all' ] }
```

License
-------

BSD

Author Information
------------------

Jakub K. Boguslaw <jboguslaw@gmail.com>. Pease inform me if you find any error/mistake
